import { createSlice } from '@reduxjs/toolkit'
import { API } from '../../api/API'

const initialState = {
  slides: [],
  isLoading: false,
}

const sliderSlice = createSlice({
  name: 'slider',
  initialState,
  reducers: {
    getSlidesRequest(state) {
      state.slides = []
      state.isLoading = true
    },
    getSlidesSuccess(state, action) {
      state.slides = action.payload
      state.isLoading = false
    },
  },
})

export const getSlides = () => {
  return async (dispatch) => {
    dispatch(sliderSlice.actions.getSlidesRequest())
    let slides = await API.getSlides()
    dispatch(sliderSlice.actions.getSlidesSuccess(slides))
  }
}

export default sliderSlice.reducer
