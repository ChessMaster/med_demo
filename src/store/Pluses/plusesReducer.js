import { createAction, createReducer } from '@reduxjs/toolkit'
import { API } from '../../api/API'

export const getPlusesRequest = createAction('Modules/GET_PLUSES_REQUEST')
export const getPlusesSuccess = createAction('Modules/GET_PLUSES_SUCCESS')

const initialState = {
  pluses: [],
  isLoading: true,
}

export const getPluses = () => {
  return async (dispatch) => {
    dispatch(getPlusesRequest())
    let pluses = await API.getPluses()
    dispatch(getPlusesSuccess(pluses))
  }
}

export const plusesReducer = createReducer(initialState, {
  [getPlusesRequest]: (state, action) => {
    state.isLoading = true
  },
  [getPlusesSuccess]: (state, action) => {
    state.pluses = action.payload
    state.isLoading = false
  },
})
