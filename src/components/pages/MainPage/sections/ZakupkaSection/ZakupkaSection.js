import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './ZakupkaSection.scss'
import TextSection from '../TextSection/TextSection'
import { Selector } from '../../../../../utils/Selectors'

const ZakupkaSection = ({ info }) => {
  return <TextSection id={'zakupka'} info={info} backgroundColor={'#F5F5F5'} />
}

ZakupkaSection.propTypes = {
  info: PropTypes.object,
}

const mstp = (state) => ({
  info: Selector.getSection(state, 'zakupka'),
})

export default connect(mstp, null, null, { pure: true })(ZakupkaSection)
