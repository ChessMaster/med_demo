import { createSlice } from '@reduxjs/toolkit'
import { API } from '../../api/API'

const initialState = {
  sections: [],
  isLoading: false,
}

const sectionsSlice = createSlice({
  name: 'sections',
  initialState,
  reducers: {
    getSectionsRequest(state) {
      state.sections = []
      state.isLoading = true
    },
    getSectionsSuccess(state, { payload }) {
      state.sections = payload
      state.isLoading = false
    },
  },
})

export const getSections = () => {
  return async (dispatch) => {
    dispatch(sectionsSlice.actions.getSectionsRequest())
    let sections = await API.getSections()
    dispatch(sectionsSlice.actions.getSectionsSuccess(sections))
  }
}

export default sectionsSlice.reducer
