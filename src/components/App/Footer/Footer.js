import React from 'react'
import style from './footer.module.scss'
import Grid from '@material-ui/core/Grid'
import Container from '@material-ui/core/Container'

const Footer = () => (
  <footer className={style.Footer}>
    <Container maxWidth={'lg'}>
      <Grid container spacing={2}>
        <Grid
          container
          item
          xs={12}
          sm={4}
          justify={'center'}
          alignItems={'center'}
        >
          <BrainSystemsLogo />
        </Grid>
        <Grid
          container
          item
          xs={12}
          sm={4}
          justify={'center'}
          alignItems={'center'}
        >
          <BrainSystemsLicense />
        </Grid>
        <Grid
          container
          item
          xs={12}
          sm={4}
          justify={'center'}
          alignItems={'center'}
        >
          <ContactInfo />
        </Grid>
      </Grid>
    </Container>
  </footer>
)

const BrainSystemsLogo = () => {
  return (
    <a href={'http://brainsystems.ru/'} className={style.Link}>
      <img
        alt={'BrainSystems'}
        src={require('./logo171_128.png')}
        height={44}
      />
      <span style={{ fontSize: 24, color: '#FFD71F' }}>Brain</span>
      <span style={{ fontSize: 24, color: '#2CA5E2' }}>Systems</span>
    </a>
  )
}

const BrainSystemsLicense = () => {
  return (
    <div style={{ fontSize: 18, color: '#939090' }}>
      BRAINSYSTEMS LLC © 2017
    </div>
  )
}

const ContactInfo = () => {
  return (
    <div style={{ fontSize: 18, color: '#cbc8c8' }}>
      <div>Контакты</div>
      <div>8(800)555-3107</div>
      <div>zakupki@brainsystems.ru</div>
    </div>
  )
}

export default Footer
