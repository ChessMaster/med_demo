import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './TextModulesSection.scss'
import { Selector } from '../../../../../utils/Selectors'
import TextSection from '../TextSection/TextSection'

const TextModulesSection = ({ info }) => {
  return <TextSection id={'modules'} info={info} backgroundColor={'#F5F5F5'} />
}

TextModulesSection.propTypes = {
  info: PropTypes.object,
}

const mstp = (state) => ({
  info: Selector.getSection(state, 'modules-2'),
})

export default connect(mstp, null, null, { pure: true })(TextModulesSection)
