export const Selector = {
  getSections: (state) => state.sections.sections,
  getSection: (state, id) => state.sections.sections.find((el) => el.id === id),
  getSlides: (state) => state.slider.slides,
  getIsSliderLoading: (state) => state.slider.isLoading,
}
