import React from 'react'
import Accordion from '@material-ui/core/Accordion'
import AccordionSummary from '@material-ui/core/AccordionSummary'
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'
import Typography from '@material-ui/core/Typography'
import AccordionDetails from '@material-ui/core/AccordionDetails'
import TableContainer from '@material-ui/core/TableContainer'
import Table from '@material-ui/core/Table'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import TableCell from '@material-ui/core/TableCell'
import TableBody from '@material-ui/core/TableBody'
import { makeStyles } from '@material-ui/core/styles'
import { withStyles } from '@material-ui/core'
import Switch from '@material-ui/core/Switch'
import { formatNumber } from '../../../utils/Utils'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    '&:last-of-type': {
      marginBottom: '10px !important',
    },
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  container: {
    marginTop: '20px',
    padding: '10px',
    borderRadius: '5px',
    backgroundColor: '#f5f5f5',
  },
  tableDetails: {
    padding: 0,
  },
  FuncName: {
    cursor: 'pointer',
  },
  FuncName_active: {
    color: '#00BCD1',
    fontWeight: 'bold',
  },
  Summary: {
    justifyContent: 'space-between',
  },
  SubSum: {
    color: '#00BCD1',
    whiteSpace: 'nowrap',
  },
}))

const OptionsAccordion = ({ data, title, setOption, subsum }) => {
  const classes = useStyles()
  return (
    <Accordion
      TransitionProps={{ unmountOnExit: true }}
      className={classes.root}
    >
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        id="panel1a-header"
        classes={{ content: classes.Summary }}
      >
        <Typography className={classes.heading}>{title}</Typography>
        <Typography className={classes.SubSum}>{subsum}</Typography>
      </AccordionSummary>
      <AccordionDetails className={classes.tableDetails}>
        <TableContainer>
          <Table size="small" aria-label="options table">
            <TableHead>
              <TableRow>
                <TableCell>Функциональные возможности</TableCell>
                <TableCell
                  style={{ width: '95px', maxWidth: '95px' }}
                  align={'center'}
                >
                  Стоимость
                </TableCell>
                <TableCell style={{ width: '50px', maxWidth: '50px' }}>
                  Выбран
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((option) => {
                const onOptionChange = () => {
                  setOption(title, option.name, !option.isChecked)
                }
                return (
                  <TableRow key={title + option.name}>
                    <TableCell
                      component="th"
                      scope="row"
                      onClick={onOptionChange}
                      className={
                        classes.FuncName +
                        ` ${option.isChecked && classes.FuncName_active}`
                      }
                    >
                      {option.name}
                    </TableCell>
                    <TableCell align={'center'}>
                      {formatNumber(option.value)}
                    </TableCell>
                    <TableCell>
                      <IOSSwitch
                        name="checkedB"
                        checked={option.isChecked}
                        onChange={onOptionChange}
                      />
                    </TableCell>
                  </TableRow>
                )
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </AccordionDetails>
    </Accordion>
  )
}

const IOSSwitch = withStyles((theme) => ({
  root: {
    width: 42,
    height: 26,
    padding: 0,
    margin: theme.spacing(1),
  },
  switchBase: {
    padding: 1,
    '&$checked': {
      transform: 'translateX(16px)',
      color: theme.palette.common.white,
      '& + $track': {
        backgroundColor: '#00BCD1',
        opacity: 1,
        border: 'none',
      },
    },
    '&$focusVisible $thumb': {
      color: '#00BCD1',
      border: '6px solid #fff',
    },
  },
  thumb: {
    width: 24,
    height: 24,
  },
  track: {
    borderRadius: 26 / 2,
    border: `1px solid ${theme.palette.grey[400]}`,
    backgroundColor: theme.palette.grey[50],
    opacity: 1,
    transition: theme.transitions.create(['background-color', 'border']),
  },
  checked: {},
  focusVisible: {},
}))(({ classes, ...props }) => {
  return (
    <Switch
      focusVisibleClassName={classes.focusVisible}
      disableRipple
      classes={{
        root: classes.root,
        switchBase: classes.switchBase,
        thumb: classes.thumb,
        track: classes.track,
        checked: classes.checked,
      }}
      {...props}
    />
  )
})

export default OptionsAccordion
