import React, { useEffect, useState } from 'react'
import Section from '../MainPage/sections/common/Section/Section'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import OptionsAccordion from './OptionsAccordion'
import { formatNumber } from '../../../utils/Utils'
import { useDispatch, useSelector } from 'react-redux'
import { getFuncGroups } from '../../../store/Functions/FunctionsReducer'

const funcGroupsJson = [
  {
    name: 'Лицензия',
    functions: [
      { name: 'Личный кабинет врача', value: '30000', isChecked: false },
      {
        name: 'Рабочее место Регистратора',
        value: '30000',
        isChecked: false,
      },
      { name: 'Рабочее место Врача', value: '30000', isChecked: false },
      {
        name: 'Рабочее место Постовой медсестры',
        value: '30000',
        isChecked: false,
      },
      {
        name: 'Рабочее место Процедурной медсестры',
        value: '30000',
        isChecked: false,
      },
      { name: 'Модуль Аптека', value: '30000', isChecked: false },
      { name: 'Модуль Больничные листы', value: '30000', isChecked: false },
      {
        name: 'Рабочее место Администратора МИС',
        value: '30000',
        isChecked: false,
      },
      {
        name: 'Рабочее место Руководителя',
        value: '30000',
        isChecked: false,
      },
    ],
  },
  {
    name: 'Интеграция с внутренними системами/оборудованием',
    functions: [
      {
        name: 'Интеграция с бухгалтерскими системами',
        value: '30000',
        isChecked: false,
      },
      {
        name: 'Интеграция с системой отдела кадров',
        value: '30000',
        isChecked: false,
      },
      {
        name: 'Интеграция с контрольно-кассовой техникой',
        value: '25000',
        isChecked: false,
      },
      {
        name: 'Интеграция с лабораторным оборудованием (за 1 ед.оборудования)',
        value: '100000',
        isChecked: false,
      },
      {
        name:
          'Интеграция с диагностическим оборудованием (за 1 ед.оборудования)',
        value: '100000',
        isChecked: false,
      },
    ],
  },
  {
    name: 'Интеграция с внешними системами',
    functions: [
      { name: 'Личный кабинет врача', value: '25000', isChecked: false },
      {
        name: 'Личный кабинет врача 2 ',
        value: '100000',
        isChecked: false,
      },
      { name: 'Личный кабинет врача 3', value: '100000', isChecked: false },
    ],
  },
]

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: '10px',
    padding: '10px',
    borderRadius: '5px',
    backgroundColor: '#f5f5f5',
  },
  sum: {
    color: '#00BCD1',
  },
}))

const CalculateValuePage = ({}) => {
  const classes = useStyles()

  const dispatch = useDispatch()
  const groups = useSelector((state) => state.functionsReducer.groups)

  useEffect(() => {
    dispatch(getFuncGroups())
  }, [])

  useEffect(() => {
    setFuncGroups(
      groups.map((group) => {
        let newFunctions = group.functions.map((func) => ({
          ...func,
          isChecked: false,
        }))
        return { ...group, functions: newFunctions }
      })
    )
  }, [groups])

  const [funcGroups, setFuncGroups] = useState([])

  const toggleFunction = (groupName, name, newValue) => {
    let targetGroupIdx = funcGroups.findIndex(
      (group) => group.name === groupName
    )
    let targetGroup = funcGroups[targetGroupIdx]
    let targetFuncIdx = targetGroup.functions.findIndex(
      (func) => func.name === name
    )
    if (targetFuncIdx !== undefined) {
      targetGroup.functions[targetFuncIdx].isChecked = newValue
    }
    let newGroups = funcGroups
    newGroups[targetGroupIdx] = targetGroup
    setFuncGroups([...newGroups])
  }

  const sum = formatNumber(
    funcGroups.reduce((sum, current) => {
      return (
        sum +
        current.functions.reduce((sum, current) => {
          if (current.isChecked) {
            return sum + Number.parseInt(current.value)
          }
          return sum
        }, 0)
      )
    }, 0)
  )

  return (
    <main>
      <Section isMaxHeight={true}>
        <div className={classes.container}>
          <Typography variant="h5" component="h1" gutterBottom>
            Расчитать стоимость
          </Typography>
          {funcGroups.map((group) => {
            let subSum = group.functions.reduce((sum, current) => {
              if (current.isChecked) {
                return sum + Number.parseInt(current.value)
              }
              return sum
            }, 0)
            if (subSum) {
              subSum = formatNumber(subSum)
            }
            return (
              <OptionsAccordion
                data={group.functions}
                title={group.name}
                setOption={toggleFunction}
                key={group.name}
                subsum={subSum || ''}
              />
            )
          })}
          <Typography variant={'h6'}>
            Итог: <span className={classes.sum}>{sum}</span>
          </Typography>
        </div>
      </Section>
    </main>
  )
}

export default CalculateValuePage
