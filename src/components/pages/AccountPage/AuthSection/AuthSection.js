import React, { useState } from 'react'
import QuestionsForm from './AuthForm/QuestionsForm'
import { createMuiTheme, ThemeProvider } from '@material-ui/core'
import styles from './AuthSection.module.scss'
import Snackbar from '@material-ui/core/Snackbar'
import Alert from '@material-ui/lab/Alert'
import Section from '../../MainPage/sections/common/Section/Section'
import { API } from '../../../../api/API'

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#62efff',
      main: '#00bcd1',
      dark: '#008ba0',
      contrastText: 'rgba(0,0,0,0.51)',
    },
  },
})

const AuthSection = () => {
  const [isAlertOpen, setIsAlertOpen] = useState(false)

  const onSubmit = (formData) => {
    API.sendMessage(formData).then((res) => {
      setIsAlertOpen(true)
    })
  }

  const handleCloseAlert = () => {
    setIsAlertOpen(false)
  }

  return (
    <div className={styles.Section}>
      <ThemeProvider theme={theme}>
        <QuestionsForm onSubmit={onSubmit} />
        <Snackbar
          open={isAlertOpen}
          autoHideDuration={4000}
          onClose={handleCloseAlert}
        >
          <Alert
            variant={'filled'}
            onClose={handleCloseAlert}
            severity="success"
          >
            Успешный вход
          </Alert>
        </Snackbar>
      </ThemeProvider>
    </div>
  )
}

export default AuthSection
