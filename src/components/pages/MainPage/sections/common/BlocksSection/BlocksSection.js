import React, { useEffect, useState } from 'react'
import Section from '../Section/Section'
import Blocks from './Blocks/Blocks'

const BlocksSection = ({
  id,
  blocks,
  info,
  getBlocks,
  backgroundColor = 'white',
  isLoading,
  isOnlyImage = false,
  linkBaseUrl = '/',
}) => {
  useEffect(() => {
    if (!blocks.length) {
      getBlocks()
    }
  }, [getBlocks])

  const [selectedBlock, setSelectedBlock] = useState('all')

  const onBlockClick = (event) => {
    setSelectedBlock(
      blocks.find((el) => String(el.id) === event.currentTarget.id)
    )
  }

  const onAllClick = () => {
    setSelectedBlock('all')
  }

  return (
    <Section
      id={id}
      isMaxHeight={true}
      title={info?.title}
      subtitle={selectedBlock === 'all' && info?.subtitle}
      keyWords={info?.title_key_words}
      backgroundColor={backgroundColor}
    >
      <Blocks
        blocks={blocks}
        id={id}
        selectedBlock={selectedBlock}
        onBlockClick={onBlockClick}
        onAllClick={onAllClick}
        isLoading={isLoading}
        isOnlyImage={isOnlyImage}
        linkBaseUrl={linkBaseUrl}
      />
    </Section>
  )
}

export default BlocksSection
