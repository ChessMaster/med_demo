import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import { modulesReducer } from './Modules/modulesReducer'
import sections from './Sections/sectionsReducer'
import slider from './Slider/sliderReducer'
import { menuReducer } from './Menu/menuReducer'
import { plusesReducer } from './Pluses/plusesReducer'
import directionsReducer from './Directions/directionsReducer'
import { reducer as formReducer } from 'redux-form'
import { functionsReducer } from './Functions/FunctionsReducer'
import { logger } from 'redux-logger'

const reducer = {
  modulesReducer,
  sections,
  slider,
  menuReducer,
  plusesReducer,
  functionsReducer,
  directionsReducer,
  form: formReducer,
}

// const middleware = [...getDefaultMiddleware(), logger]
const middleware = [...getDefaultMiddleware()]

export const store = configureStore({
  reducer,
  middleware,
  devTools: process.env.NODE_ENV !== 'production',
})
