import React from 'react'
import AuthSection from './AuthSection/AuthSection'

const AccountPage = () => (
  <main>
    <AuthSection />
  </main>
)

export default AccountPage
