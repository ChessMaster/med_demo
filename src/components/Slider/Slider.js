import React, { useEffect } from 'react'
import style from './slider.module.css'
import { Carousel } from 'react-bootstrap'
import Anchor from '../common/Anchor/Anchor'
import { connect } from 'react-redux'
import { getSlides } from '../../store/Slider/sliderReducer'
import Skeleton from '@material-ui/lab/Skeleton'
import { Selector } from '../../utils/Selectors'

const Slider = ({ slides, isLoading, getSlides }) => {
  useEffect(() => {
    if (!slides.length) {
      getSlides()
    }
  }, [])

  return (
    <>
      <Anchor id={'slider'} />
      {isLoading ? (
        <Skeleton animation="wave" height={400} variant="rect" />
      ) : (
        <Carousel className={style.Slider} interval={7000}>
          {slides.map((slide) => {
            return (
              <Carousel.Item key={slide.title} style={{ height: 400 }}>
                <img
                  className={style.Image}
                  src={slide.image}
                  alt={slide.title}
                  height={400}
                  loading="lazy"
                />
                <Carousel.Caption className={style.Caption}>
                  {slide.title && <h3>{slide.title}</h3>}
                  {slide.subtitle && <p>{slide.subtitle}</p>}
                </Carousel.Caption>
              </Carousel.Item>
            )
          })}
        </Carousel>
      )}
    </>
  )
}

const mstp = (state) => ({
  slides: Selector.getSlides(state),
  isLoading: Selector.getIsSliderLoading(state),
})

export default connect(mstp, { getSlides }, null, { pure: true })(Slider)
