import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { useScrollTrigger } from '@material-ui/core'
import Slide from '@material-ui/core/Slide'
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward'
import EmailIcon from '@material-ui/icons/Email'
import { HashLink } from 'react-router-hash-link'
import Fab from '@material-ui/core/Fab'
import { createMuiTheme, ThemeProvider } from '@material-ui/core'

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#62efff',
      main: '#00bcd1',
      dark: '#004551',
      contrastText: 'rgba(0,0,0,0.68)',
    },
    secondary: {
      light: '#8dff83',
      main: '#57d153',
      dark: '#0c9f22',
      contrastText: 'rgba(0,0,0,0.68)',
    },
  },
})

function HideOnScrollTop(props) {
  const { children, window } = props
  const trigger = useScrollTrigger({ target: window ? window() : undefined })

  return (
    <Slide appear={false} direction="up" in={!trigger} timeout={500}>
      {children}
    </Slide>
  )
}

function HideOnScrollDown(props) {
  const { children, window } = props
  const trigger = useScrollTrigger({ target: window ? window() : undefined })

  return (
    <Slide appear={false} direction="up" in={trigger} timeout={500}>
      {children}
    </Slide>
  )
}

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'fixed',
    bottom: 15,
    right: 15,
    display: 'flex',
  },
  button: {
    '&:focus': {
      outline: 0,
    },
  },
}))

const FloatButtons = () => {
  const styles = useStyles()
  return (
    <>
      <HideOnScrollTop>
        <HashLink className={styles.root} smooth to={'/#slider'}>
          <Fab
            className={styles.button}
            size="large"
            color="primary"
            aria-label="scroll top"
          >
            <ArrowUpwardIcon />
          </Fab>
        </HashLink>
      </HideOnScrollTop>
      <HideOnScrollDown>
        <HashLink className={styles.root} smooth to={'/#questions'}>
          <Fab
            className={styles.button}
            size="large"
            color="secondary"
            aria-label="questions"
          >
            <EmailIcon />
          </Fab>
        </HashLink>
      </HideOnScrollDown>
    </>
  )
}

const FloatButtonWithTheme = () => (
  <ThemeProvider theme={theme}>
    <FloatButtons />
  </ThemeProvider>
)

export default FloatButtonWithTheme
