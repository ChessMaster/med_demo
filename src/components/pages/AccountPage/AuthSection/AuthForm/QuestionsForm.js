import React from 'react'
import TextField from '@material-ui/core/TextField'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import { Field, reduxForm } from 'redux-form'
import withWidth from '@material-ui/core/withWidth/withWidth'
import { isWidthUp } from '@material-ui/core'

const validate = (values) => {
  const errors = {}
  const requiredFields = ['name']
  requiredFields.forEach((field) => {
    if (!values[field]) {
      errors[field] = 'Обязательное поле'
    }
  })
  if (
    values.email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
  ) {
    errors.email = 'Некорректный email адрес'
  }
  if (values.phone && !/^[+]?\d[\d() -]{0,14}\d$/i.test(values.phone)) {
    errors.phone = 'Некорректный номер телефона'
  }

  if (!values.phone && !values.email) {
    errors.phone = 'Необходимо указать email или телефон'
  }

  return errors
}

const renderTextField = ({
  label,
  input,
  meta: { touched, invalid, error },
  ...custom
}) => (
  <TextField
    label={label}
    error={touched && invalid}
    helperText={touched && error}
    {...input}
    {...custom}
  />
)

const useStyles = makeStyles((theme) => ({
  root: {
    width: 600,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  title: {
    color: theme.palette.primary.main,
    textAlign: 'center',
    width: '100%',
    userSelect: 'none',
  },
  input: {
    height: '100px',
  },
  button: {
    backgroundColor: '#00bcd1 !important',
    '&:hover': {
      backgroundColor: '#008ba0 !important',
    },
  },
}))

const QuestionsForm = ({ handleSubmit, pristine, submitting, width }) => {
  const styles = useStyles()
  return (
    <Grid container justify={'center'} style={{ alignSelf: 'center' }}>
      <form
        className={styles.root}
        autoComplete={'off'}
        onSubmit={handleSubmit}
      >
        <Typography
          variant={isWidthUp('sm', width) ? 'h3' : 'h4'}
          className={styles.title}
        >
          АВТОРИЗАЦИЯ
        </Typography>
        <Field
          name="email"
          component={renderTextField}
          label="Email"
          variant={'filled'}
        />
        <Field
          name="password"
          component={renderTextField}
          label="Пароль"
          variant={'filled'}
          type={'password'}
        />
        <Button
          className={styles.button}
          variant={'contained'}
          type="submit"
          // disabled={pristine || submitting}
        >
          Войти
        </Button>
      </form>
    </Grid>
  )
}

export default reduxForm({
  form: 'contact',
  validate,
})(withWidth()(QuestionsForm))
