import { ApiConnect } from './ApiConnect'
import axios from 'axios'

export const API = {
  getSections() {
    return ApiConnect.get('/sections').then((res) => {
      return res.data.map((el) => ({ ...el.acf, content: el.content.rendered }))
    })
  },
  getSlides() {
    return ApiConnect.get('/slides').then((res) => {
      return res.data.map((el) => ({
        title: el.acf.title,
        subtitle: el.acf.subtitle,
        image: el.acf.image,
      }))
    })
  },
  getMenu() {
    return ApiConnect.get('/menu').then((res) => {
      return res.data.map((el) => ({
        title: el.title.rendered,
        url: el.acf.url,
      }))
    })
  },
  getPluses() {
    return selectModule('/pluses')
  },
  getModules() {
    return ApiConnect.get('/modules').then((res) => {
      return res.data.map((module) => {
        return {
          id: module.acf?.module_id,
          title: module.title?.rendered,
          content: module.content?.rendered,
          excerpt: module.acf?.short_info,
          image: module.acf.image,
        }
      })
    })
  },
  getSubModules() {
    return ApiConnect.get('/submodules/?per_page=100').then((res) => {
      return res.data.map((module) => {
        return {
          id: module.id,
          parentId: module.acf?.module_id,
          title: module.title?.rendered,
          content: module.content?.rendered,
          excerpt: module.acf?.short_info,
          image: module.acf.image,
        }
      })
    })
  },
  getDirections() {
    return selectModule('/directions')
  },
  sendMessage(formData) {
    let contactFormData = new FormData()
    contactFormData.append('your-name', formData.name)
    contactFormData.append('your-email', formData.email || '')
    contactFormData.append('your-phone', formData.phone || '')
    contactFormData.append('your-message', formData.message || '')
    return axios({
      method: 'post',
      url:
        'http://10.0.1.136:8000/wp-json/contact-form-7/v1/contact-forms/203/feedback',
      data: contactFormData,
      headers: { 'Content-Type': 'multipart/form-data' },
    })
  },
  getFunctions() {
    return ApiConnect.get('/functions/?per_page=100').then((res) => {
      const functions = res.data.map((func) => {
        return {
          name: func.title?.rendered,
          group: func.acf?.group,
          value: func.acf?.value,
        }
      })
      return functions
    })
  },
}

const selectModule = (endpoint) => {
  return ApiConnect.get(endpoint).then((res) => {
    return res.data.map((module) => {
      return {
        id: module.id,
        title: module.title?.rendered,
        content: module.content?.rendered,
        excerpt: module.acf?.short_info,
        image: module.acf.image,
      }
    })
  })
}
