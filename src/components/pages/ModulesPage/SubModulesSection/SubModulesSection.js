import React, { useCallback, useEffect } from 'react'
import { connect } from 'react-redux'
import { useLocation, useParams, useHistory } from 'react-router-dom'
import BlocksSection from '../../MainPage/sections/common/BlocksSection/BlocksSection'
import { getSubModules } from '../../../../store/Modules/modulesReducer'

const SubModulesSection = ({ subModules, getSubModules, isLoading }) => {
  let { id, title } = useParams()
  let { pathname } = useLocation()
  let history = useHistory()
  useEffect(() => {
    window.onpopstate = onBackPage
    return () => {
      window.onpopstate = null
    }
  }, [history])

  const onBackPage = useCallback(
    () => {
      history.goBack()
    },
    [history],
  )


  return (
    <BlocksSection
      id={'submodules'}
      info={{ title: title }}
      getBlocks={getSubModules}
      blocks={subModules.filter((el) => el.parentId === id)}
      backgroundColor={'#F5F5F5'}
      isLoading={isLoading}
      linkBaseUrl={pathname}
    />
  )
}

const mstp = (state) => ({
  subModules: state.modulesReducer.subModules,
  isLoading: state.modulesReducer.isLoading,
})

export default connect(mstp, { getSubModules }, null, { pure: true })(
  SubModulesSection
)
