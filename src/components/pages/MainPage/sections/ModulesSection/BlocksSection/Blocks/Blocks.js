import React from 'react'
import Grid from '@material-ui/core/Grid'
import styles from './Blocks.module.scss'
import Skeleton from '@material-ui/lab/Skeleton'
import 'react-medium-image-zoom/dist/styles.scss'
import CardCell from '../../../common/BlocksSection/Blocks/CardCell/CardCell'
import { Link } from 'react-router-dom'

const Blocks = ({ blocks = [], id, isLoading = false }) => {
  return (
    <Grid container spacing={2}>
      {isLoading ? (
        <>
          {new Array(5).fill(0).map((el, idx) => (
            <Grid
              key={idx}
              item
              xs={12}
              sm={6}
              md={4}
              style={{ overflowY: 'auto' }}
            >
              <Skeleton
                variant={'rect'}
                animation={'wave'}
                height={285}
                width={'100%'}
              />
            </Grid>
          ))}
        </>
      ) : (
        <>
          {blocks.map((el) => {
            return (
              <Grid
                key={el.id}
                item
                xs={12}
                sm={6}
                md={4}
                style={{ overflowY: 'auto', display: 'flex' }}
              >
                <Link
                  className={styles.Link}
                  to={`/modules/${el.title}/${el.id}`}
                >
                  <CardCell
                    title={el.title}
                    excerpt={el.excerpt}
                    image={el.image}
                    onClick={null}
                    id={el.id}
                  />
                </Link>
              </Grid>
            )
          })}
        </>
      )}
    </Grid>
  )
}

export default Blocks
