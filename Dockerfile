FROM mhart/alpine-node:11 AS builder
WORKDIR /medsphere
COPY . .
RUN yarn run build

FROM mhart/alpine-node
RUN yarn global add serve
WORKDIR /medsphere
COPY --from=builder /medsphere/build .
CMD ["serve", "-p", "3030", "-s", "."]
