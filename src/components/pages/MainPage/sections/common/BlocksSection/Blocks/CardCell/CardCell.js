import React from 'react'
import Card from '@material-ui/core/Card'
import CardActionArea from '@material-ui/core/CardActionArea'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'
import styles from './CardCell.module.css'

const CardCell = ({ title, image, excerpt, onClick, id }) => {
  return (
    <Card id={id} className={styles.Card} onClick={onClick}>
      <CardActionArea>
        <CardMedia
          component="img"
          alt={title}
          height="140"
          image={image}
          title={title}
        />
        <CardContent>
          <Typography
            className={`${styles.Text} ${styles.Title}`}
            gutterBottom
            variant="h5"
            component="h2"
          >
            {title}
          </Typography>
          {/*<Typography*/}
          {/*  className={`${styles.Text} ${styles.Subtitle}`}*/}
          {/*  variant="body2"*/}
          {/*  color="textSecondary"*/}
          {/*  component="p"*/}
          {/*>*/}
          {/*  {excerpt}*/}
          {/*</Typography>*/}
        </CardContent>
      </CardActionArea>
    </Card>
  )
}

CardCell.propTypes = {}

export default CardCell
