import React from 'react'
import { connect } from 'react-redux'
import BlocksSection from '../common/BlocksSection/BlocksSection'
import { getDirections } from '../../../../../store/Directions/directionsReducer'

const DirectionsSection = ({ info, directions, getDirections, isLoading }) => {
  return (
    <BlocksSection
      id={'directions'}
      info={info}
      getBlocks={getDirections}
      blocks={directions}
      isLoading={isLoading}
      isOnlyImage={true}
      backgroundColor={'#F5F5F5'}
    />
  )
}

const mstp = (state) => ({
  info: state.sections.sections.find((el) => el.id === 'directions'),
  directions: state.directionsReducer.directions,
  isLoading: state.directionsReducer.isLoading,
})

export default connect(mstp, { getDirections }, null, { pure: true })(
  DirectionsSection
)
