import React from 'react'
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid'
import style from './section.module.scss'
import Anchor from '../../../../../common/Anchor/Anchor'
import Container from '@material-ui/core/Container'
import Typography from '@material-ui/core/Typography'
import withWidth, { isWidthUp } from '@material-ui/core/withWidth'
import Skeleton from '@material-ui/lab/Skeleton'
import { connect } from 'react-redux'

const Section = ({
  id,
  children,
  backgroundColor = 'white',
  isMaxHeight = false,
  title,
  subtitle,
  keyWords = '',
  customClassName = '',
  width,
  isLoading,
}) => {
  let formattedSubtitle = <div className={style.SubTitle}>{subtitle}</div>
  if (keyWords && subtitle) {
    const subtitleArray = subtitle.split(keyWords)
    formattedSubtitle = (
      <Typography
        variant={isWidthUp('sm', width) ? 'h3' : 'h4'}
        gutterBottom
        className={style.SubTitle}
      >
        {subtitleArray[0]}
        <span className={style.keyWords}>{keyWords}</span>
        {subtitleArray[1]}
      </Typography>
    )
  }
  return (
    <Grid
      className={`${style.Section} ${
        isMaxHeight && style.maxHeight
      } ${customClassName}`}
      container
      direction="row"
      justify="center"
      alignItems="flex-start"
      style={{ backgroundColor: backgroundColor }}
    >
      <Container maxWidth={'lg'}>
        <Anchor id={id} />
        {isLoading ? (
          <>
            <Skeleton variant={'text'} height={90} animation={'wave'} />
            <Skeleton variant={'text'} height={90} animation={'wave'} />
          </>
        ) : (
          <>
            {title && (
              <Typography
                variant={isWidthUp('sm', width) ? 'h3' : 'h4'}
                gutterBottom
                className={style.Title}
              >
                {title}
              </Typography>
            )}
            {subtitle && formattedSubtitle}
            {children}
          </>
        )}
      </Container>
    </Grid>
  )
}

Section.propTypes = {
  minHeight: PropTypes.string,
  backgroundColor: PropTypes.string,
}

const mstp = (state) => ({
  isLoading: state.sections.isLoading,
})

export default connect(mstp)(withWidth()(Section))
