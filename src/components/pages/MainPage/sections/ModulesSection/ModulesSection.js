import React from 'react'
import { connect } from 'react-redux'
import BlocksSection from './BlocksSection/BlocksSection'
import { getModules } from '../../../../../store/Modules/modulesReducer'
import { Selector } from '../../../../../utils/Selectors'

const ModulesSection = ({ info, modules, getModules, isLoading }) => {
  return (
    <BlocksSection
      id={'modules'}
      info={info}
      blocks={modules}
      getBlocks={getModules}
      isLoading={isLoading}
    />
  )
}

const mstp = (state) => ({
  info: Selector.getSection(state, 'modules'),
  modules: state.modulesReducer.modules,
  isLoading: state.modulesReducer.isLoading,
})

export default connect(mstp, { getModules }, null, { pure: true })(
  ModulesSection
)
