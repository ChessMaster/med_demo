import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './EquipmentSection.scss'
import { Selector } from '../../../../../utils/Selectors'
import TextSection from '../TextSection/TextSection'

const EquipmentSection = ({ info }) => {
  return <TextSection info={info} backgroundColor={'white'} id={'equipment'} />
}

EquipmentSection.propTypes = {
  info: PropTypes.object,
}

const mstp = (state) => ({
  info: Selector.getSection(state, 'equipment'),
})

export default connect(mstp, null, null, { pure: true })(EquipmentSection)
