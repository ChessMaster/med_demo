import React, { useEffect } from 'react'
import Blocks from './Blocks/Blocks'
import Section from '../../common/Section/Section'

const BlocksSection = ({
  id,
  blocks,
  info,
  getBlocks,
  backgroundColor = 'white',
  isLoading,
  isOnlyImage = false,
  linkBaseUrl = '',
}) => {
  useEffect(() => {
    if (!blocks.length) {
      getBlocks()
    }
  }, [getBlocks])

  return (
    <Section
      id={id}
      isMaxHeight={true}
      title={info?.title}
      backgroundColor={backgroundColor}
    >
      <Blocks
        blocks={blocks}
        id={id}
        isLoading={isLoading}
        isOnlyImage={isOnlyImage}
        linkBaseUrl={linkBaseUrl}
      />
    </Section>
  )
}

export default BlocksSection
