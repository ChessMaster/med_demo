import React from 'react'
import style from './content.module.scss'

import { Switch, Route } from 'react-router-dom'
import AccountPage from '../../pages/AccountPage/AccountPage'
import MainPage from '../../pages/MainPage/MainPage'
import ModulesPage from '../../pages/ModulesPage/ModulesPage'
import CalculateValuePage from '../../pages/CalculateValuePage/CalculateValuePage'

const ContentSwitcher = () => {
  return (
    <div className={style.Content}>
      <Switch>
        <Route path={'/account'}>
          <AccountPage />
        </Route>
        <Route path={'/calc'}>
          <CalculateValuePage />
        </Route>
        <Route path={'/modules/:title/:id'}>
          <ModulesPage />
        </Route>
        <Route path={'/'}>
          <MainPage />
        </Route>
      </Switch>
    </div>
  )
}

export default ContentSwitcher
