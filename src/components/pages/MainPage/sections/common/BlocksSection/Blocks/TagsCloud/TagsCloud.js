import React from 'react'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core'

const TagsCloud = ({ tags, onTagClick, activeTagId, onAllClick }) => {
  return (
    <div style={{ marginBottom: '0.35em' }}>
      {tags.map((el) => (
        <Tag
          key={el.id}
          title={el.title}
          onClick={onTagClick}
          id={el.id}
          isActive={el.id === activeTagId}
        />
      ))}
      <Tag
        title={'< Все >'}
        onClick={onAllClick}
        id={'all'}
        isActive={false}
        isAll={true}
      />
    </div>
  )
}

const ColorButton = withStyles((theme) => ({
  root: {
    color: theme.palette.getContrastText('#00BCD1'),
    backgroundColor: 'white',
    '&:hover,&:focus, &:active': {
      color: '#00BCD1',
      borderColor: '#00BCD1',
      outline: 'none',
      backgroundColor: 'white',
    },
    borderRadius: '50px',
    marginRight: '10px',
    marginBottom: '10px',
  },
}))(Button)

const ActiveButton = withStyles((theme) => ({
  root: {
    color: '#00BCD1',
    backgroundColor: 'white',
    '&:hover,&:focus, &:active': {
      color: '#00BCD1',
      borderColor: '#00BCD1',
      outline: 'none',
      backgroundColor: 'white',
    },
    borderColor: '#00BCD1',
    borderRadius: '50px',
    marginRight: '10px',
    marginBottom: '10px',
  },
}))(Button)

const AllButton = withStyles((theme) => ({
  root: {
    color: '#11D100',
    backgroundColor: 'white',
    '&:hover,&:focus, &:active': {
      color: '#11D100',
      borderColor: '#11D100',
      outline: 'none',
      backgroundColor: 'white',
    },
    borderColor: '#11D100',
    borderRadius: '50px',
    marginRight: '10px',
    marginBottom: '10px',
  },
}))(Button)

const Tag = ({ title, onClick, id, isActive, isAll = false }) => {
  return (
    <>
      {isAll ? (
        <AllButton variant={'contained'} id={id} onClick={onClick}>
          {title}
        </AllButton>
      ) : (
        <>
          {isActive ? (
            <ActiveButton variant={'contained'} id={id} onClick={onClick}>
              {title}
            </ActiveButton>
          ) : (
            <ColorButton variant={'contained'} id={id} onClick={onClick}>
              {title}
            </ColorButton>
          )}
        </>
      )}
    </>
  )
}

TagsCloud.propTypes = {}

export default TagsCloud
