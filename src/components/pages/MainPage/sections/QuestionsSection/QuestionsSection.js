import React, { useState } from 'react'
import Section from '../common/Section/Section'
import QuestionsForm from './QuestionsForm/QuestionsForm'
import { createMuiTheme, ThemeProvider } from '@material-ui/core'
import styles from './QuestionsSection.module.css'
import { API } from '../../../../../api/API'
import Snackbar from '@material-ui/core/Snackbar'
import Alert from '@material-ui/lab/Alert'

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#62efff',
      main: '#00bcd1',
      dark: '#008ba0',
      contrastText: 'rgba(0,0,0,0.51)',
    },
    type: 'dark',
  },
})

const QuestionsSection = () => {
  const [isAlertOpen, setIsAlertOpen] = useState(false)

  const onSubmit = (formData) => {
    API.sendMessage(formData).then((res) => {
      setIsAlertOpen(true)
    })
  }

  const handleCloseAlert = () => {
    setIsAlertOpen(false)
  }

  return (
    <Section
      customClassName={styles.Section}
      id={'questions'}
      backgroundColor={'#1D1D1D'}
    >
      <ThemeProvider theme={theme}>
        <QuestionsForm onSubmit={onSubmit} />
        <Snackbar
          open={isAlertOpen}
          autoHideDuration={4000}
          onClose={handleCloseAlert}
        >
          <Alert
            variant={'filled'}
            onClose={handleCloseAlert}
            severity="success"
          >
            Сообщение успешно отправлено
          </Alert>
        </Snackbar>
      </ThemeProvider>
    </Section>
  )
}

export default QuestionsSection
