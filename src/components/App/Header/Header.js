import React, { useEffect, useState } from 'react'
import { Nav, Navbar, NavLink } from 'react-bootstrap'
import { connect } from 'react-redux'
import { getMenu } from '../../../store/Menu/menuReducer'
import { NavHashLink } from 'react-router-hash-link'
import styles from './Header.module.scss'
import Typography from '@material-ui/core/Typography'

const Header = ({ menu, getMenu }) => {
  useEffect(() => {
    if (!menu.length) {
      getMenu()
    }
  }, [getMenu])

  const [isExpand, setIsExpand] = useState(false)

  const onNavLinkClick = () => {
    setIsExpand(false)
  }

  const toggleExpand = () => {
    setIsExpand(!isExpand)
  }

  return (
    <Navbar
      fixed="top"
      bg="light"
      expand="xl"
      expanded={isExpand}
      className={styles.Header}
    >
      <Navbar.Brand>
        <NavHashLink className={styles.Brand} smooth to={'/#home'}>
          <img
            alt={'МедСфера'}
            src={require('./logo2.png')}
            height={36}
            width={36}
            style={{ marginRight: 2 }}
          />
          <Typography
            style={{ color: 'rgba(0,0,0,0.7)', fontWeight: 'bold' }}
            variant={'h6'}
          >
            <div style={{ lineHeight: '20px' }}>МедСфера</div>
          </Typography>
        </NavHashLink>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" onClick={toggleExpand} />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="ml-auto">
          {menu.map((el) => {
            let link = (
              <NavHashLink
                smooth
                to={el.url}
                key={el.url}
                onClick={onNavLinkClick}
                className={styles.Link}
              >
                {el.title}
              </NavHashLink>
            )
            if (el.url.startsWith('http')) {
              link = (
                <NavLink href={el.url} className={styles.Link} key={el.url}>
                  {el.title}
                </NavLink>
              )
            }
            return <React.Fragment key={el.url}>{link}</React.Fragment>
          })}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}

const mstp = (state) => ({
  menu: state.menuReducer.menu,
})

export default connect(mstp, { getMenu })(Header)
