import React from 'react'
import QuestionsSection from '../MainPage/sections/QuestionsSection/QuestionsSection'
import FloatButtons from '../../common/FloatButtons/FloatButtons'
import SubModulesSection from './SubModulesSection/SubModulesSection'

const ModulesPage = ({}) => {
  return (
    <main>
      <SubModulesSection info={{ title: 'test' }} />
      <QuestionsSection />
      <FloatButtons />
    </main>
  )
}

export default ModulesPage
