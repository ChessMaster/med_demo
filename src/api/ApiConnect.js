import * as axios from 'axios'

export const BACKEND_SOURCE = 'http://10.0.1.136:8000/wp-json/wp/v2/'

export const ApiConnect = axios.create({
  baseURL: BACKEND_SOURCE,
  withCredentials: true,
})
