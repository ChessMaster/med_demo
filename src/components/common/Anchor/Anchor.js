import React from 'react'
import style from './anchor.module.scss'

const Anchor = ({ id, offset = -78 }) => (
  <a className={style.Anchor} id={id} href={`#${id}`} style={{ top: offset }} />
)

export default Anchor
