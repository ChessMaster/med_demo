import React, { useEffect } from 'react'
import Slider from '../../Slider/Slider'
import AboutSection from './sections/AboutSection/AboutSection'
import EquipmentSection from './sections/EquipmentSection/EquipmentSection'
import QuestionsSection from './sections/QuestionsSection/QuestionsSection'
import PlusesSection from './sections/PlusesSection/PlusesSection'
import { connect } from 'react-redux'
import { getSections } from '../../../store/Sections/sectionsReducer'
import FloatButtons from '../../common/FloatButtons/FloatButtons'
import DirectionsSection from './sections/DirectionsSection/DirectionsSection'
import Anchor from '../../common/Anchor/Anchor'
import TextModulesSection from './sections/TextModulesSeсtion/TextModulesSection'
import ZakupkaSection from './sections/ZakupkaSection/ZakupkaSection'
import { Selector } from '../../../utils/Selectors'

const MainPage = ({ sections, getSections }) => {
  useEffect(() => {
    if (!sections.length) {
      getSections()
    }
  }, [getSections])

  return (
    <main>
      <Anchor id={'home'} />
      <Slider />
      <AboutSection />
      <ZakupkaSection />
      <PlusesSection />
      <DirectionsSection />
      <EquipmentSection />
      {/*<ModulesSection />*/}
      <TextModulesSection />
      {/*<TestModulesSection/>*/}
      <QuestionsSection />
      <FloatButtons />
    </main>
  )
}

const mstp = (state) => ({
  sections: Selector.getSections(state),
})

export default connect(mstp, { getSections }, null, { pure: true })(MainPage)
