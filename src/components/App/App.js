import React from 'react'
import './App.css'
import Header from './Header/Header'
import ContentSwitcher from './ContentSwitcher/ContentSwitcher'
import Footer from './Footer/Footer'

function App() {
  return (
    <div className="App">
      <Header />
      <ContentSwitcher />
      <Footer />
    </div>
  )
}

export default App
