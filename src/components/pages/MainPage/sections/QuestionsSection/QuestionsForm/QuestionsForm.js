import React, { useState } from 'react'
import MaskedInput from 'react-text-mask'
import TextField from '@material-ui/core/TextField'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button'
import { Field, reduxForm } from 'redux-form'
import withWidth from '@material-ui/core/withWidth/withWidth'
import { isWidthUp } from '@material-ui/core'
import InputLabel from '@material-ui/core/InputLabel'
import Input from '@material-ui/core/Input'
import FormControl from '@material-ui/core/FormControl'
import FilledInput from '@material-ui/core/FilledInput'
import FormHelperText from '@material-ui/core/FormHelperText'

const validate = (values) => {
  const errors = {}
  const requiredFields = ['name']
  requiredFields.forEach((field) => {
    if (!values[field]) {
      errors[field] = 'Обязательное поле'
    }
  })
  if (
    values.email &&
    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
  ) {
    errors.email = 'Некорректный email адрес'
  }
  let phone = values.phone
  if (phone) {
    phone = phone.replaceAll('x', '')
  }

  if (phone && /^[\+7]+[\d]{0,9}$/i.test(phone)) {
    errors.phone = 'Некорректный номер телефона'
  }

  if ((!phone || phone === '+7') && !values.email) {
    errors.email = 'Необходимо указать email или телефон'
  }

  return errors
}

const renderTextField = ({
  label,
  input,
  meta: { touched, invalid, error },
  ...custom
}) => (
  <TextField
    label={label}
    error={touched && invalid}
    helperText={touched && error}
    {...input}
    {...custom}
  />
)

function TextMaskCustom(props) {
  const { inputRef, ...other } = props

  return (
    <MaskedInput
      {...other}
      ref={(ref) => {
        inputRef(ref ? ref.inputElement : null)
      }}
      mask={[
        '+',
        '7',
        /[1-9]/,
        /\d/,
        /\d/,
        /\d/,
        /\d/,
        /\d/,
        /\d/,
        /\d/,
        /\d/,
        /\d/,
      ]}
      placeholderChar={'x'}
      showMask
      type={'filled'}
    />
  )
}

const renderNumberField = ({
  label,
  input,
  meta: { touched, invalid, error },
  ...custom
}) => {
  return (
    <FormControl>
      <InputLabel htmlFor="phone-number-input">{label}</InputLabel>
      <FilledInput
        name="textmask"
        id="phone-number-input"
        inputComponent={TextMaskCustom}
        error={touched && invalid}
        // helperText={touched && error}
        value={'+7         '}
        {...input}
        {...custom}
      />
      <FormHelperText
        style={{
          color: '#f44336',
          marginLeft: '14px',
          marginRight: '14px',
        }}
      >
        {touched && error}
      </FormHelperText>
    </FormControl>
  )
}

const useStyles = makeStyles((theme) => ({
  root: {
    width: 600,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  title: {
    color: theme.palette.primary.main,
    textAlign: 'center',
    width: '100%',
    userSelect: 'none',
  },
  input: {
    height: '100px',
  },
  button: {
    backgroundColor: '#00bcd1',
    '&:hover': {
      backgroundColor: '#008ba0',
    },
  },
}))

const QuestionsForm = ({ handleSubmit, pristine, submitting, width }) => {
  const styles = useStyles()
  return (
    <Grid container justify={'center'}>
      <form
        className={styles.root}
        autoComplete={'off'}
        onSubmit={handleSubmit}
      >
        <Typography
          variant={isWidthUp('sm', width) ? 'h3' : 'h4'}
          className={styles.title}
        >
          ---------
        </Typography>
        <Typography
          variant={isWidthUp('sm', width) ? 'h3' : 'h4'}
          className={styles.title}
        >
          ЕСТЬ ВОПРОСЫ?
        </Typography>
        <Field
          name="name"
          component={renderTextField}
          label="Ваше имя"
          variant={'filled'}
          required
        />
        <Field
          name="email"
          component={renderTextField}
          label="Email"
          variant={'filled'}
        />
        <Field
          name="phone"
          component={renderNumberField}
          label="Телефон"
          variant={'filled'}
        />
        <Field
          name="message"
          component={renderTextField}
          label="Сообщение"
          variant={'filled'}
          multiline
          rows={5}
          rowsMax={15}
          required
        />
        <Button
          className={styles.button}
          variant={'contained'}
          type="submit"
          disabled={pristine || submitting}
        >
          Отправить
        </Button>
      </form>
    </Grid>
  )
}

export default reduxForm({
  form: 'contact',
  validate,
})(withWidth()(QuestionsForm))
