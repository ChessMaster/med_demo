import { createAction, createReducer } from '@reduxjs/toolkit'
import { API } from '../../api/API'

export const getFunctionsRequest = createAction(
  'Sections/GET_FUNCTIONS_REQUEST'
)
export const getFunctionsSuccess = createAction(
  'Sections/GET_FUNCTIONS_SUCCESS'
)

export const getFuncGroups = () => {
  return async (dispatch) => {
    dispatch(getFunctionsRequest())
    let functions = await API.getFunctions()
    let funcGroups = []
    functions.forEach((func) => {
      if (!funcGroups.find((group) => group.name === func.group)) {
        funcGroups.push({ name: func.group, functions: [] })
      }
      const groupIdx = funcGroups.findIndex(
        (group) => group.name === func.group
      )
      funcGroups[groupIdx].functions.push({
        name: func.name,
        value: func.value,
      })
    })
    dispatch(getFunctionsSuccess(funcGroups))
  }
}

const initialState = {
  groups: [],
}

export const functionsReducer = createReducer(initialState, {
  [getFunctionsRequest]: (state, action) => {
    state.groups = []
  },
  [getFunctionsSuccess]: (state, action) => {
    state.groups = action.payload
  },
})
