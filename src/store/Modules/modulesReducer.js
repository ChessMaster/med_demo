import { createAction, createReducer } from '@reduxjs/toolkit'
import { API } from '../../api/API'

export const getModulesRequest = createAction('Modules/GET_MODULES_REQUEST')
export const getModulesSuccess = createAction('Modules/GET_MODULES_SUCCESS')
export const getSubModulesRequest = createAction(
  'Modules/GET_SUB_MODULES_REQUEST'
)
export const getSubModulesSuccess = createAction(
  'Modules/GET_SUB_MODULES_SUCCESS'
)

const initialState = {
  modules: [],
  subModules: [],
  isLoading: true,
}

export const getModules = () => {
  return async (dispatch) => {
    dispatch(getModulesRequest())
    let modules = await API.getModules()
    dispatch(getModulesSuccess(modules))
  }
}
export const getSubModules = () => {
  return async (dispatch) => {
    dispatch(getSubModulesRequest())
    let subModules = await API.getSubModules()
    dispatch(getSubModulesSuccess(subModules))
  }
}

export const modulesReducer = createReducer(initialState, {
  [getModulesRequest]: (state) => {
    state.isLoading = true
  },
  [getModulesSuccess]: (state, action) => {
    state.modules = action.payload
    state.isLoading = false
  },
  [getSubModulesRequest]: (state) => {
    state.isLoading = true
  },
  [getSubModulesSuccess]: (state, action) => {
    state.subModules = action.payload
    state.isLoading = false
  },
})
