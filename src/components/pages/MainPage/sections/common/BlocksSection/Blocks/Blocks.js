import React from 'react'
import Grid from '@material-ui/core/Grid'
import CardCell from './CardCell/CardCell'
import styles from './Blocks.module.scss'
import TagsCloud from './TagsCloud/TagsCloud'
import Paper from '@material-ui/core/Paper/Paper'
import { HashLink } from 'react-router-hash-link'
import Skeleton from '@material-ui/lab/Skeleton'
import Zoom from 'react-medium-image-zoom'
import 'react-medium-image-zoom/dist/styles.css'

const Blocks = ({
  blocks = [],
  id,
  selectedBlock,
  onBlockClick,
  onAllClick,
  isLoading = false,
  isOnlyImage = false,
  linkBaseUrl = '/',
}) => {
  return (
    <>
      {selectedBlock === 'all' ? (
        <Grid container spacing={2}>
          {isLoading ? (
            <>
              {new Array(5).fill(0).map((el, idx) => (
                <Grid
                  key={idx}
                  item
                  xs={12}
                  sm={6}
                  md={4}
                  style={{ overflowY: 'auto' }}
                >
                  <Skeleton
                    variant={'rect'}
                    animation={'wave'}
                    height={285}
                    width={'100%'}
                  />
                </Grid>
              ))}
            </>
          ) : (
            <>
              {blocks.map((el) => {
                return (
                  <Grid
                    key={el.id}
                    item
                    xs={12}
                    sm={6}
                    md={4}
                    style={{ overflowY: 'auto', display: 'flex' }}
                  >
                    <HashLink
                      className={styles.Link}
                      to={linkBaseUrl + `#${id}`}
                    >
                      <CardCell
                        title={el.title}
                        excerpt={el.excerpt}
                        image={el.image}
                        onClick={onBlockClick}
                        id={el.id}
                      />
                    </HashLink>
                  </Grid>
                )
              })}
            </>
          )}
        </Grid>
      ) : (
        <div className={styles.PlusContent}>
          {/*<Anchor id={`${id}-start`} offset={-85}/>*/}
          <TagsCloud
            tags={blocks}
            onTagClick={onBlockClick}
            onAllClick={onAllClick}
            activeTagId={selectedBlock.id}
          />
          {isOnlyImage ? (
            <ZoomImage
              title={selectedBlock.title}
              styles={styles}
              image={selectedBlock.image}
            />
          ) : (
            <ImageContent
              title={selectedBlock.title}
              image={selectedBlock.image}
              styles={styles}
              htmlContent={selectedBlock.content}
            />
          )}
        </div>
      )}
    </>
  )
}

const ImageContent = ({ title, image, styles, htmlContent }) => {
  return (
    <Paper style={{ borderRadius: '10px', padding: 5 }} elevation={3}>
      <img
        alt={title}
        src={image}
        width={'100%'}
        height={300}
        style={{ objectFit: 'cover', borderRadius: 10 }}
      />
      {/*<Typography variant={'h4'}>{selectedBlock.title}</Typography>*/}
      <div
        className={styles.Content}
        dangerouslySetInnerHTML={{ __html: htmlContent }}
      />
    </Paper>
  )
}

const ZoomImage = ({ title, image }) => {
  return (
    <Paper style={{ borderRadius: '10px', padding: 5 }} elevation={3}>
      <Zoom>
        <img
          alt={title}
          src={image}
          width={'100%'}
          style={{ borderRadius: 10 }}
        />
      </Zoom>
    </Paper>
  )
}

export default Blocks
