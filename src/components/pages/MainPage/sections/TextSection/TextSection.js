import React from 'react'
import Section from '../common/Section/Section'
import Typography from '@material-ui/core/Typography'
import style from '../AboutSection/about.module.scss'
import './TextSection.scss'

const TextSection = ({ info, backgroundColor, id }) => {
  return (
    <Section
      id={id}
      isMaxHeight={true}
      backgroundColor={backgroundColor}
      title={info?.title}
      subtitle={info?.subtitle}
      keyWords={info?.title_key_words}
    >
      {info && info.content && (
        <Typography
          className={style.Content}
          dangerouslySetInnerHTML={{ __html: info.content }}
        />
      )}
    </Section>
  )
}

export default TextSection
