import { createSlice } from '@reduxjs/toolkit'
import { API } from '../../api/API'

const initialState = {
  directions: [],
  isLoading: true,
}

export const getDirections = () => {
  return async (dispatch) => {
    dispatch(directionsSlice.actions.getDirectionsRequest())
    let directions = await API.getDirections()
    dispatch(directionsSlice.actions.getDirectionsSuccess(directions))
  }
}

const directionsSlice = createSlice({
  name: 'directions',
  initialState,
  reducers: {
    getDirectionsRequest(state) {
      state.isLoading = true
    },
    getDirectionsSuccess(state, action) {
      state.directions = action.payload
      state.isLoading = false
    },
  },
})

export default directionsSlice.reducer
