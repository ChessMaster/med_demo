import { createAction, createReducer } from '@reduxjs/toolkit'
import { API } from '../../api/API'

export const getMenuRequest = createAction('Sections/GET_MENU_REQUEST')
export const getMenuSuccess = createAction('Sections/GET_MENU_SUCCESS')

export const getMenu = () => {
  return async (dispatch) => {
    dispatch(getMenuRequest())
    let menu = await API.getMenu()
    dispatch(getMenuSuccess(menu))
  }
}

const initialState = {
  menu: [],
}

export const menuReducer = createReducer(initialState, {
  [getMenuRequest]: (state, action) => {
    state.menu = []
  },
  [getMenuSuccess]: (state, action) => {
    state.menu = action.payload
  },
})
