import React from 'react'
import { connect } from 'react-redux'
import BlocksSection from '../common/BlocksSection/BlocksSection'
import { getPluses } from '../../../../../store/Pluses/plusesReducer'
import { Selector } from '../../../../../utils/Selectors'

const PlusesSection = ({ info, pluses, getPluses, isLoading }) => {
  return (
    <BlocksSection
      id={'pluses'}
      info={info}
      getBlocks={getPluses}
      blocks={pluses}
      backgroundColor={'white'}
      isLoading={isLoading}
    />
  )
}

const mstp = (state) => ({
  info: Selector.getSection(state, 'pluses'),
  pluses: state.plusesReducer.pluses,
  isLoading: state.plusesReducer.isLoading,
})

export default connect(mstp, { getPluses }, null, { pure: true })(PlusesSection)
