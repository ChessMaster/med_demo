import React from 'react'
import PropTypes from 'prop-types'
import Section from '../common/Section/Section'
import { connect } from 'react-redux'
import style from './about.module.scss'
import Typography from '@material-ui/core/Typography'
import { Selector } from '../../../../../utils/Selectors'

const AboutSection = ({ info }) => {
  return (
    <Section
      id={'about'}
      title={info?.title}
      subtitle={info?.subtitle}
      keyWords={info?.title_key_words}
      customClassName={style.AboutSection}
    >
      {info && info.content && (
        <Typography
          className={style.Content}
          dangerouslySetInnerHTML={{ __html: info.content }}
        />
      )}
    </Section>
  )
}

AboutSection.propTypes = {
  info: PropTypes.object,
}

const mstp = (state) => ({
  info: Selector.getSection(state, 'about'),
})

export default connect(mstp, null, null, { pure: true })(AboutSection)
